$("document").ready(function(){
  $("#radioSim").click(function(){
    $("#adicionarInstituicao").fadeIn(300);
    $("#cadastrarVoluntario").hide();
  });
  $("#radioNao").click(function(){
    $("#adicionarInstituicao").hide();
    $("#cadastrarVoluntario").fadeIn(300);
    $(".esconderNovaInstituicao").fadeOut(300);
    $("#adicionarOutraInstituicao").hide();
    $("#cadastrarVoluntariado").hide();
  });

  $("#adicionarInstituicao").click(function(){
    $("#novaInstituicao").fadeIn(300);
    $("#adicionarInstituicao").fadeOut(300);
    $("#adicionarOutraInstituicao").fadeIn(300);
    $("#cadastrarVoluntariado").fadeIn(300);
  });

  $("#OutroInteresse").click(function(){
    $("#form-widgets-outro").fadeToggle(300);
  });

  $("#form-widgets-voluntario-desde-0").attr("placeholder", "mm/aaaa");
  $("#form-widgets-voluntario-desde-0").mask("99/9999");

  var id = 1;

  $("#adicionarOutraInstituicao").click(function(){

    $('<div class= "alert alert-dismissible esconderNovaInstituicao" role="alert" id="novaInstituicao-'+id+'">' +
    '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
    '<h3 class="azulsenado l-letter">Dados da Instituição</h3>' +
    '<div class="row">' +
      '<div class="col-md-6">' +
        '<div class="form-group">' +
          '<label for="form-widgets-nome-instituicao">Nome<span class="text-danger">*</span></label><span>' +
            '<input id="form-widgets-nome-instituicao" name="form.widgets.nome-instituicao" value="" type="text"' + 'class="text-widget required textline-field form-control"/></span>' +
        '</div>' +
      '</div>' +
      '<div class="col-md-6">' +
        '<div class="form-group">' +
          '<label for="form-widgets-voluntario-desde-' + id +'">Voluntário desde</label><span>' +
            '<input id="form-widgets-voluntario-desde-' + id +'" name="form.widgets.voluntario-desde" value="" type="text"' + 'class="text-widget textline-field form-control"/></span>' +
        '</div>' +
      '</div>' +
    '</div>'+
    '<div class="row">' +
      '<div class="col-md-6">' +
        '<div class="form-group">' +
          '<label for="form-widgets-cidade">Cidade<span class="text-danger">*</span></label><span>' +
            '<input id="form-widgets-cidade" name="form.widgets.cidade" value="" type="text" class="text-widget required' + 'textline-field form-control"/></span>' +
        '</div>' +
      '</div>' +
      '<div class="col-md-6">' +
        '<div class="form-group">' +
          '<label for="form-widgets-estado">Estado</label><span>' +
            '<input id="form-widgets-estado" name="form.widgets.estado" value="" type="text" class="text-widget textline-field form-control"/></span>' +
        '</div>' +
      '</div>' +
    '</div>' +
    '<div class="row">' +
      '<div class="col-md-6">' +
        '<div class="form-group">' +
          '<label for="form-widgets-telefone">Telefone<span class="text-danger">*</span></label><span>' +
            '<input id="form-widgets-telefone" name="form.widgets.telefone" value="" type="text" class="text-widget required textline-field form-control"/></span>' +
        '</div>' +
      '</div>' +
      '<div class="col-md-6">' +
        '<div class="form-group">' +
          '<label for="form-widgets-email">E-mail</label><span>' +
            '<input id="form-widgets-email" name="form.widgets.email" value="" type="text" class="text-widget textline-field form-control"/></span>' +
        '</div>' +
      '</div>' +
    '</div>').hide().appendTo("#novaInstituicaoContainer").fadeIn(300);
    $("#form-widgets-voluntario-desde-" + id).attr("placeholder", "mm/aaaa");
    $("#form-widgets-voluntario-desde-" + id).mask("99/9999");
    id++;
  });

  $("#radioNao").click(function(){
      $("#nova")
  });
});
